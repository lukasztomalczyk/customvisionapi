﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ComputerVisionAPI.Model;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;

namespace ComputerVisionAPI.Services
{
    public class VisionService: IVisionService
    {
        private string _key = Environment.GetEnvironmentVariable("COMPUTERVISION_KEY");
        private string _url = Environment.GetEnvironmentVariable("COMPUTERVISION_URL");
        
        private const string remoteImageUrl =
            "http://upload.wikimedia.org/wikipedia/commons/3/3c/Shaki_waterfall.jpg";
        
        private static readonly List<VisualFeatureTypes> features =
            new List<VisualFeatureTypes>()
            {
                VisualFeatureTypes.Categories, VisualFeatureTypes.Description,
                VisualFeatureTypes.Faces, VisualFeatureTypes.ImageType,
                VisualFeatureTypes.Tags
            };

        public async Task<Result> CheckImage(string url)
        {
            ComputerVisionClient computerVision = new ComputerVisionClient(
                new ApiKeyServiceClientCredentials(_key));

            computerVision.Endpoint = _url;
            var result = await AnalyzeRemoteAsync(computerVision, url);
            
            return result;
        }
        
        private static async Task<Result> AnalyzeRemoteAsync(ComputerVisionClient computerVision, string imageUrl)
        {
            if (!Uri.IsWellFormedUriString(imageUrl, UriKind.Absolute))
            {
                throw new Exception("invalid image url");
            }

            ImageAnalysis analysis =
                await computerVision.AnalyzeImageAsync(imageUrl, features);
            return MakeResult(analysis);
        }

        private static Result MakeResult(ImageAnalysis input)
        {
            return new Result()
            {
                Categories = input.Categories[0].Name,
                Accuracy = (float)input.Categories[0].Score,
                Description = input.Description.Captions[0].Text,
                Tags = input.Tags.ToList()
            };
        }
    }
}