﻿using System.Threading.Tasks;
using ComputerVisionAPI.Model;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;

namespace ComputerVisionAPI.Services
{
    public interface IVisionService
    {
        Task<Result> CheckImage(string url);
    }
}