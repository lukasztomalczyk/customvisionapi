﻿using System;
using System.Threading.Tasks;
using ComputerVisionAPI.Model;
using ComputerVisionAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace ComputerVisionAPI.Controllers
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class VisionController : Controller
    {
        private readonly IVisionService _visionService;

        public VisionController(IVisionService visionService)
        {
            _visionService = visionService;
        }
        
        [HttpPost]
        public async Task<ActionResult<Result>> Check([FromBody] UrlImage url)
        {
            var result = await _visionService.CheckImage(url.Url);
            return result;
        }
    }
}