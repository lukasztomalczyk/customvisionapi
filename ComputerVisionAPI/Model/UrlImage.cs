﻿namespace ComputerVisionAPI.Model
{
    public class UrlImage
    {
        public string Url { get; set; }
    }
}