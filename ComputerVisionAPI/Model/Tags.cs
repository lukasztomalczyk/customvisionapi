﻿namespace ComputerVisionAPI.Model
{
    public class Tags
    {
        public string Name { get; set; }
        public float Confidence { get; set; }
    }
}