﻿using System.Collections.Generic;
using Microsoft.Azure.CognitiveServices.Vision.ComputerVision.Models;

namespace ComputerVisionAPI.Model
{
    public class Result
    {
        public string Categories { get; set; }
        public string Description { get; set; }
        public float Accuracy { get; set; }
        public List<ImageTag> Tags { get; set; }
    }
}